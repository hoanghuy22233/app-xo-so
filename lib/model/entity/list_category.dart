class CategoryList {
  final String title, img, name, link;
  final int id;

  CategoryList({this.title, this.img, this.name, this.link, this.id});
}

List<CategoryList> categoryList = [
  CategoryList(
      name: "Miền Nam",
      id: 1,
      img: "assets/images/resutl.jpg",
      link: "https://ketqua.net/xo-so-mien-nam"),
  CategoryList(
      name: "Miền Bắc",
      id: 2,
      img: "assets/images/resutl.jpg",
      link: "https://ketqua.net/xo-so-mien-bac"),
  CategoryList(
      name: "Miền Trung",
      id: 3,
      img: "assets/images/resutl.jpg",
      link: "https://ketqua.net/xo-so-mien-trung"),
  CategoryList(
    name: "DS chiết khấu",
    id: 4,
    img: "assets/images/img_gift.png",
  ),
];
