class NewsList {
  final String date, img, name, content, location;
  final int id;

  NewsList(
      {this.date, this.img, this.name, this.content, this.id, this.location});
}

List<NewsList> newsList = [
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
  NewsList(
      name: "Đăng thống kế ngày 12/03",
      content: "25,26,26/10k",
      id: 1,
      location: "hải phòng",
      img: "assets/images/lo.jpg",
      date: "2 giờ trước"),
];
