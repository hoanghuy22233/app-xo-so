import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/widget_profile_menu.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/account/widget_account_appbar.dart';
import 'package:base_code_project/presentation/screen/account/widget_profile_infor.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _buildAppbar(),Expanded(child: _buildMenu())
          ],
        ));
  }

  Widget _buildAppbar() => WidgetAccountAppbar();

  Widget _buildMenu() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            WidgetProfileInfor(
              name:
              AppLocalizations.of(context).translate('profile.no_name'),
              phone:
              AppLocalizations.of(context)
                  .translate('profile.no_setup'),
              avatar: Image.asset("assets/images/promotion_value_png.png",height: 60,width: 60,),
              onTap: () {
                //  AppNavigator.navigateProfileDetail();
              },
            ),
            WidgetProfileMenu(
              text: AppLocalizations.of(context)
                  .translate('profile.order_history'),
              image: Image.asset('assets/images/img_order_history.png',width: 30,height: 30,),
              onTap: () {
                // AppNavigator.navigateOrderHistory();
              },
            ),
            WidgetProfileMenu(
              text: AppLocalizations.of(context)
                  .translate('profile.address_book'),
              image: Image.asset('assets/images/img_address_book.png',width: 30,height: 30,),
              onTap: () {
                // AppNavigator.navigateAddress();
              },
            ),
            WidgetProfileMenu(
              text:
              AppLocalizations.of(context).translate('profile.contact'),
              image: Image.asset('assets/images/img_contact.png',width: 30,height: 30,),
              onTap: () {
                // AppNavigator.navigateContact();
              },
            ),
            WidgetProfileMenu(
              text: AppLocalizations.of(context)
                  .translate('profile.question'),
              image: Image.asset('assets/images/img_question.png',width: 30,height: 30,),
              onTap: () {
                // AppNavigator.navigateQuestion();
              },
            ),
            WidgetProfileMenu(
              text: AppLocalizations.of(context)
                  .translate('profile.term_and_policy'),
              image: Image.asset('assets/images/img_term_and_policy.png',width: 30,height: 30,),
              onTap: () {
                // AppNavigator.navigateTerm();
              },
            ),
            WidgetProfileMenu(
              text: AppLocalizations.of(context)
                  .translate('profile.change_password'),
              image: Image.asset('assets/images/img_change_password.png',width: 30,height: 30,),
              onTap: () {
                //  AppNavigator.navigateChangePassword();
              },
            ),
            WidgetProfileMenu(
              text:
              AppLocalizations.of(context).translate('profile.logout'),
              image: Image.asset('assets/images/img_logout.png',width: 30,height: 30,),
              onTap: ()  {
                AppNavigator.navigateLoginPage();
              },
            ),
            WidgetSpacer(
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
