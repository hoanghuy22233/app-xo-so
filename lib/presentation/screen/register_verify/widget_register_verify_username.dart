
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/utils/common/common_utils.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetRegisterVerifyUsername extends StatelessWidget {
  final String username;

  const WidgetRegisterVerifyUsername({Key key, @required this.username}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context).translate('register_verify.otp'),
          style: AppStyle.DEFAULT_LARGE,
        ),
        WidgetSpacer(height: 15,),
        Text(
          '${AppCommonUtils.hideUserName(username)}',
          style: AppStyle.DEFAULT_MEDIUM,
        ),
        WidgetSpacer(height: 10,),
        Text(
          AppLocalizations.of(context).translate('register_verify_notifications'),
          style: AppStyle.DEFAULT_LARGE,
        ),
      ],
    );
  }
}
