import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/presentation/screen/animation/ani.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ForgotPassVerifyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.red,
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 400,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage('assets/images/logo_remove.png'),
                  )),
                ),
                FadeAnimation(
                  1.5,
                  Container(
                    child: Text(
                      "Mã xác nhận đã được gửi tới email:",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
                FadeAnimation(
                  1.5,
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      "tai****@gmail.com",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(30.0),
                  child: Column(
                    children: <Widget>[
                      FadeAnimation(
                          1.5,
                          Container(
                            padding: EdgeInsets.only(left: 20),
                            child: _buildCodeField(),
                          )),
                      SizedBox(
                        height: 30,
                      ),
                      FadeAnimation(
                          2,
                          GestureDetector(
                            onTap: () {
                              AppNavigator.navigateForgotPasswordResetPage();
                            },
                            child: Container(
                              height: 50,
                              child: Card(
                                elevation: 5,
                                color: Colors.red,
                                shape: StadiumBorder(
                                  side: BorderSide(
                                    color: Colors.white,
                                    width: 2,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate('register_verify.continue'),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )),
                      SizedBox(
                        height: 70,
                      ),
                      FadeAnimation(
                          1.5,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppLocalizations.of(context).translate(
                                    'forgot_password_verify.no_code'),
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Text(
                                  AppLocalizations.of(context).translate(
                                      'forgot_password_verify.resend'),
                                  style: TextStyle(
                                    color: Colors.white,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget _buildCodeField() {
    return PinCodeTextField(
      onChanged: (changed) {},
      length: 6,
      obsecureText: false,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderWidth: 2,
        inactiveColor: Colors.red,
        inactiveFillColor: AppColor.GREY_LIGHTER_3,
        selectedColor: AppColor.GREY,
        selectedFillColor: AppColor.GREY,
        activeColor: AppColor.GREY_LIGHTER_3,
        activeFillColor: AppColor.GREY_LIGHTER_3,
      ),
      animationDuration: Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      textInputType: TextInputType.phone,
      controller: null,
      onCompleted: (completed) {
        print("Completed: $completed");
      },
      beforeTextPaste: (text) {
        return true;
      },
    );
  }
}
