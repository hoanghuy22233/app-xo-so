import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:flutter/material.dart';

class WidgetButtonPost extends StatelessWidget {
  final Function onTap;
  final String text;
  final isEnable;

  const WidgetButtonPost({Key key, this.onTap, this.text, this.isEnable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: isEnable ? Colors.white : AppColor.BUTTON_DISABLE_COLOR,
          child: Center(
              child: Text(
                text,
                style: isEnable
                    ? AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.RED)
                    : AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.BLACK),
              )),
        ),
      ),
    );
  }
}
