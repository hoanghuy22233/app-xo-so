import 'dart:io';

import 'package:base_code_project/presentation/screen/post_news/widget_button_post.dart';
import 'package:base_code_project/presentation/screen/post_news/widget_form_post_input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


class WidgetPostsFormScreen extends StatefulWidget {
  @override
  _WidgetPostsFormScreenState createState() => _WidgetPostsFormScreenState();
}

class _WidgetPostsFormScreenState extends State<WidgetPostsFormScreen> {
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;

  bool _isButtonDisabled;

  final ImagePicker picker = ImagePicker();

  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _descriptionTextController = TextEditingController();
  @override
  void initState() {
    super.initState();

    _isButtonDisabled = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.red,
              height: 89,
              // color: Colors.black,
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 20, top: 20),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Image.asset(
                              "assets/images/left_arrow.png",
                              height: 25,
                              width: 25,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Positioned.fill(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: 20, top: 20),
                          child: _buildSubmitButton(),
                        )
                      ],
                    ),
                  ),
                  Positioned.fill(
                      child: FractionallySizedBox(
                        widthFactor: .5,
                        child: Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Tạo bài viết",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              )),
                        ),
                      ))
                ],
              ),
            ),
            Divider(
              height: 1,
              thickness: 1,
              color: Colors.black12,
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 20),
              child: WidgetFormInput(
                controller: _nameTextController,
                text: '',
                hint: "Tiêu đề",
                // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
              ),
            ),
            Divider(
              height: 1,
              thickness: 1,
              color: Colors.black12,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: WidgetFormInput(
                controller: _descriptionTextController,
                text: '',
                hint: "Nhập thông tin đăng phiếu?",
                // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
              ),
            ),
            croppedFile != null
                ? Container(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: Image.file(
                croppedFile,
                fit: BoxFit.cover,
              ),
            )
                : Container(),
            SizedBox(
              height: 20,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Tooltip(
                    message: "Đính kèm",
                    child: MaterialButton(
                      onPressed: () {
                        _getImageFromGallery();
                      },
                      color: Colors.red,
                      textColor: Colors.white,
                      child: Image.asset(
                        "assets/images/image-gallery.png",
                        scale: 1.5,
                      ),
                      padding: EdgeInsets.all(10),
                      shape: CircleBorder(),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () {
                    },
                    color: Colors.red,
                    textColor: Colors.white,
                    child: Image.asset(
                      "assets/images/photo.png",
                      scale: 1.5,
                    ),
                    padding: EdgeInsets.all(10),
                    shape: CircleBorder(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildSubmitButton() {
    return WidgetButtonPost(
      onTap: () {
        _isButtonDisabled = false;
      },
      isEnable: _isButtonDisabled,
      text: "Đăng",
    );
  }
  Future<Null> _getImageFromGallery() async {
    croppedFile = null;
    final pickedFile = await _picker.getImage(source: ImageSource.gallery,imageQuality: 25);
    setState(() {
      croppedFile = File(pickedFile.path);
    });
  }
  @override
  void dispose() {
    _nameTextController.dispose();
    _descriptionTextController.dispose();
    super.dispose();
  }


}






