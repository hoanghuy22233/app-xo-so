import 'dart:io';

import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class LoginOrRegister extends StatefulWidget {
  @override
  _LoginOrRegisterState createState() => _LoginOrRegisterState();
}

class _LoginOrRegisterState extends State<LoginOrRegister> {

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:WillPopScope(
        onWillPop: _onWillPop,
        child:  Stack(
          alignment: Alignment.center,
          children: [
            Image.asset("assets/images/background_linh_anh.jpg",width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,fit: BoxFit.fill,),
            Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height/5,
                ),
                Container(
                  height: MediaQuery.of(context).size.width/2,
                  width: MediaQuery.of(context).size.width/2,
                  child: Image.asset("assets/images/logo_linh_anh.png"),
                ),
                SizedBox(height: 20,),
                Container(
                  width: MediaQuery.of(context).size.width -50,
                  height: AppValue.ACTION_BAR_HEIGHT,
                  child: GestureDetector(
                    onTap: () {
                      AppNavigator.navigateLogin();
                    },
                    child: Card(
                      elevation: 2,
                      shape:
                      StadiumBorder(
                        side: BorderSide(
                          color: Colors.red,
                          width: 0.5,
                        ),
                      ),
                      color: Colors.red,
                      child: Center(
                          child: Text(
                            "ĐĂNG NHẬP",
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width -50,
                  height: AppValue.ACTION_BAR_HEIGHT,
                  child: GestureDetector(
                    onTap: () {
                      AppNavigator.navigateRegister();
                    },
                    child: Card(
                      elevation: 2,
                      shape:
                      StadiumBorder(
                        side: BorderSide(
                          color: Colors.red,
                          width: 0.5,
                        ),
                      ),
                      color: Colors.white,
                      child: Center(
                          child: Text(
                            "ĐĂNG KÝ",
                            style: TextStyle(color: Colors.red),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ],
        )
      ),
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Bạn chắc chứ?'),
        content: Text('Bạn muốn thoát ứng dụng?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Không'),
          ),
          FlatButton(
            onPressed: () => exit(0),
            /*Navigator.of(context).pop(true)*/
            child: Text('Có'),
          ),
        ],
      ),
    ) ??
        false;
  }
}