import 'package:base_code_project/model/entity/news_list.dart';
import 'package:base_code_project/presentation/screen/detail_news_list/widget_news_list_detail_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewsDetailNewsPage extends StatefulWidget {
  final int id;
  NewsDetailNewsPage({Key key, this.id}) : super(key: key);
  _NewsDetailNewsPageState createState() => new _NewsDetailNewsPageState();
}

class _NewsDetailNewsPageState extends State<NewsDetailNewsPage> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetNewsListDetailAppbar(),
          Expanded(
              child: ListView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.vertical,
            children: [
              Container(
                child: Image.asset(
                  "${newsList[widget.id].img}",
                  height: MediaQuery.of(context).size.height / 1.5,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Text(
                  "${newsList[widget.id].name}",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),

              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    'assets/images/goldal.jpg',
                    height: 15,
                    width: 15,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    newsList[widget.id].location,
                    style: TextStyle(
                      fontSize: 10,
                    ),
                    maxLines: 3,
                  ),
                ],
              ),

              //padding text////
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Container(
                  height: isReadDetail ? null : 50,
                  child: Text(
                    newsList[widget.id].content,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              newsList[widget.id].content.length > 100
                  ? Column(
                      children: [
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isReadDetail = !isReadDetail;
                              });
                            },
                            child: isReadDetail
                                ? Text(
                                    "Thu gọn",
                                    style: TextStyle(color: Colors.blue),
                                  )
                                : Text(
                                    "Xem thêm",
                                    style: TextStyle(color: Colors.blue),
                                  ),
                          ),
                        ),
                      ],
                    )
                  : SizedBox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      newsList[widget.id].date,
                      style: TextStyle(
                        fontSize: 10,
                      ),
                      maxLines: 3,
                    ),
                  )
                ],
              ),
            ],
          ))
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
