import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/entity/list_category.dart';
import 'package:base_code_project/model/entity/list_new.dart';
import 'package:base_code_project/presentation/common_widgets/widget_profile_menu.dart';
import 'package:base_code_project/presentation/screen/home/widget_list_category.dart';
import 'package:base_code_project/presentation/screen/new/widget_list_new.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  width: MediaQuery.of(context).size.width,
                  child: Swiper(
                    autoplay: true,
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return Image.asset(
                        "assets/images/banner_xoso.jpg",
                        fit: BoxFit.cover,
                      );
                    },
                    pagination: new SwiperPagination(
                      builder: DotSwiperPaginationBuilder(
                          size: 5,
                          activeSize: 10,
                          color: AppColor.BANNER_COLOR,
                          activeColor: AppColor.BANNER_SELECTED_COLOR),
                    ),
                  ),
                ),
                Card(
                  elevation: 2,
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/logo_remove.png",
                              height: 30,
                              width: 30,
                            ),
                            Text(
                              AppLocalizations.of(context)
                                  .translate('home.list_result'),
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      GridView.builder(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return Container(
                              child: WigetCategory(
                                id: index,
                              ),
                            );
                          },
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                          ),
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: categoryList.length)
                    ],
                  ),
                ),
                Card(
                  child: Column(
                    children: [
                      WidgetProfileMenu(
                        text: AppLocalizations.of(context)
                            .translate('navigation.news'),
                        image: Image.asset(
                          "assets/images/logo_remove.png",
                          height: 30,
                          width: 30,
                        ),
                        onTap: () {
                          AppNavigator.navigateNewPage();
                        },
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 3,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: newList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              child: WigetListNew(
                                id: index,
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
