import 'package:base_code_project/model/entity/list_category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WigetCategory extends StatelessWidget {
  final int id;
  WigetCategory({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: () => Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => DetailScreen(id: id)),
      // ),
      child: Container(
        height: MediaQuery.of(context).size.height / 5,
        child: Column(
          children: [
            Container(
                height: 60,
                width: 60,
                child: Image.asset(
                  "${categoryList[id].img}",
                  fit: BoxFit.cover,
                )),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Container(
                child: Text(
                  "${categoryList[id].name}",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 10,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
