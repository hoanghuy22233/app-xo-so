import 'dart:io';

import 'package:base_code_project/app/auth_bloc/authentication_bloc.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/repo/user_repository.dart';
import 'package:base_code_project/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:base_code_project/presentation/screen/login/widget_login_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: BlocProvider(
            create: (context) => LoginBloc(
                userRepository: userRepository,
                authenticationBloc:
                BlocProvider.of<AuthenticationBloc>(context)),
            child: Column(
              children: [
                Container(
                  child: Stack(
                    children: [
                      Container(
                        color: Colors.red,
                        width: MediaQuery.of(context).size.width,
                        height: 80,
                      ),
                      Positioned.fill(
                          top: 24,
                          left: 15,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/left_arrow.png',
                                width: 25,
                                height: 25,
                              ),
                            ],
                          )),
                      Positioned.fill(
                          top: 24,
                          child: Center(
                            child: Text(
                              "Đăng nhập",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
                Expanded(
                    child: ListView(
                      padding: EdgeInsets.zero,
                      scrollDirection: Axis.vertical,
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Image.asset(
                          "assets/images/logo_linh_anh.png",
                          height: 150,
                          width: 150,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        _buildLoginForm(),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 40),
                          child: Column(
                            children: [
                              Text('Hoặc đăng nhập bằng'),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/images/fb.png',
                                    height: 40,
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Image.asset(
                                    'assets/images/google.png',
                                    height: 40,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Không có tài khoản? ',
                                    style: TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      AppNavigator.navigateRegister();
                                    },
                                    child: Text(
                                      'Đăng ký',
                                      style: TextStyle(decoration: TextDecoration.underline),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ))
              ],
            )
        ),
      ),
    );


  }

  _buildLoginForm() => WidgetLoginForm();
}
