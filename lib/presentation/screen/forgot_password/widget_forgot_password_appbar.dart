import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetForgotPasswordAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Quên mật khẩu",
        left: [WidgetAppbarMenuBack()],
      ),
    );
  }
}
