import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/model/repo/user_repository.dart';
import 'package:base_code_project/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:base_code_project/presentation/screen/forgot_password/widget_forgot_password_appbar.dart';
import 'package:base_code_project/presentation/screen/forgot_password/widget_forgot_password_form.dart';
import 'package:base_code_project/presentation/screen/forgot_password/widget_forgot_password_title.dart';
import 'package:base_code_project/presentation/screen/register/widget_bottom_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_bloc.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        child: Scaffold(
          body: BlocProvider(
            create: (context) =>
                ForgotPasswordBloc(userRepository: userRepository),
            child: Container(
                color: AppColor.PRIMARY_BACKGROUND,
                child: Column(
                  children: [
                    _buildAppbar(),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Image.asset(
                              "assets/images/logo_linh_anh.png",
                              height: 150,
                              width: 150,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _buildForgotPasswordForm(),
                            _buildBottomLogin()
                          ],
                        ),
                      )
                    ),


                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetForgotPasswordAppbar();

  _buildTop() => WidgetForgotPasswordTitle();

  _buildForgotPasswordForm() => WidgetForgotPasswordForm();
  _buildBottomLogin() => WidgetBottomLogin();
}
