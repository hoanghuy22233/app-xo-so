import 'package:base_code_project/model/entity/list_new.dart';
import 'package:base_code_project/presentation/screen/new/widget_new_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'list_new_verical.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        body: Column(
          children: <Widget>[
            WidgetNewsAppbar(),
            Expanded(
                child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: newList.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: WigetListNewVerical(
                    id: index,
                  ),
                );
              },
            ))
          ],
        ));
  }
}
