import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/model/entity/list_new.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/detail_new/sc_new_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WigetListNewVerical extends StatelessWidget {
  final int id;
  WigetListNewVerical({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NewsDetailPage(id: id)),
      ),
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: [
              Container(
                height: 80,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AspectRatio(
                      aspectRatio: AppValue.BANNER_RATIO,
                      child: Image.asset(
                        newList[id].img,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              newList[id].name,
                              style: AppStyle.DEFAULT_SMALL.copyWith(
                                  color: AppColor.THIRD_COLOR,
                                  fontWeight: FontWeight.bold),
                            ),
                            WidgetSpacer(
                              height: 3,
                            ),
                            Expanded(
                              child: Container(
                                  child: Text(
                                newList[id].content.length <= 70
                                    ? newList[id].content
                                    : newList[id].content.substring(0, 70) +
                                        '...',
                                style: AppStyle.DEFAULT_SMALL
                                    .copyWith(color: AppColor.GREY),
                                textAlign: TextAlign.start,
                              )),
                            ),
                            WidgetSpacer(
                              height: 3,
                            ),
                            Text(
                              newList[id].date,
                              style: AppStyle.DEFAULT_SMALL.copyWith(
                                  color: AppColor.GREY,
                                  fontStyle: FontStyle.italic),
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              WidgetSpacer(
                height: 10,
              ),
              Divider(
                height: 1,
              )
            ],
          ),
        ),
      ),
    );
  }
}
