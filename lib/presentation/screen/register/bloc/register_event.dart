import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

class UsernameChanged extends RegisterEvent {
  final String username;

  const UsernameChanged({@required this.username});

  @override
  List<Object> get props => [username];

  @override
  String toString() {
    return 'UsernameChanged{email: $username}';
  }
}

class PasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;

  PasswordChanged({@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'PasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class ConfirmPasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;

  ConfirmPasswordChanged(
      {@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'ConfirmPasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class PhoneNumberChanged extends RegisterEvent {
  final String phoneNumber;

  const PhoneNumberChanged({@required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];

  @override
  String toString() {
    return 'PhoneNumberChanged{email: $phoneNumber}';
  }
}

class EmailChanged extends RegisterEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() {
    return 'EmailChanged{email: $email}';
  }
}

class RegisterSubmitted extends RegisterEvent {
  final String username;
  final String password;
  final String confirmPassword;
  final String phoneNumber;
  final String email;

  const RegisterSubmitted({
    @required this.username,
    @required this.password,
    @required this.confirmPassword,
    @required this.phoneNumber,
    @required this.email,
  });

  @override
  List<Object> get props => [username, password, confirmPassword, phoneNumber, email];

  @override
  String toString() {
    return 'Submitted{email: $username, password: $password, confirmPassword: $confirmPassword, phoneNumber: $phoneNumber, email: $email}';
  }
}