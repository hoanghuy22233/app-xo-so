import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';

class WidgetProfileMenu extends StatelessWidget {
  final Widget image;
  final String text;
  final String endText;
  final Function onTap;

  const WidgetProfileMenu(
      {Key key, this.image, this.text, this.endText, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(right: 10),
                      child: image),
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: Text(
                        text,
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Text(
                        endText ?? '',
                        style: AppStyle.DEFAULT_MEDIUM_BOLD,
                      ),
                    ),
                  ),
                  WidgetSpacer(
                    width: 5,
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: AppColor.GREY,
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Divider(
              color: Colors.grey,
              height: 1,
            ),
          ),
        ],
      ),
    );
  }
}
